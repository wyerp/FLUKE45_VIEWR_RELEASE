/*
 * fluke_viewer_txy.h
 *
 *  Created on: 2015年5月11日
 *      Author: acer
 */

#ifndef FLUKE_VIEWER_TXY_H_
#define FLUKE_VIEWER_TXY_H_

#include <msp430f6638.h>
#include "stdio.h"

/********************初始化设备管脚、晶振等***************************/
void initFVT();

/*****************绑定要监视的数据***************************/
void bindDataFVT(volatile double *sendData);

#endif /* FLUKE_VIEWER_TXY_H_ */
