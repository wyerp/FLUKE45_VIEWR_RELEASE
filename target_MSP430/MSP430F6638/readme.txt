==============================================================================
fluke45 viewer target 430 驱动库使用说明


使用方法
	1.首先参看本级目录样例代码main.c
	2.如果您使用的是CCS，请在您的项目上右键-Preference-Build-Advanced Options-Library Function Assumptions
	  中的Level of printf/scanf support required (--printf_support)选择为full,否则程序使用TI精简过的stdlib.h
	  编译，造成sprintf不能正常工作，此时您在串口调试助手中发送命令“c”，将会遇到只能收到“f=>”回应。
	3.函数解释：
	/********************初始化设备管脚、晶振等***************************/
	void initFVT();
	本函数将初始化您的P82和P83作为串口收发端口，并设置波特率为9600，如使用其他单片机，请对本部分进行相关设置修改。

	/*****************绑定要监视的数据***************************/
	void bindDataFVT(double *sendData);
	您只需在主程序中使用该函数将一个需要监视的全局变量绑定一次，程序就可以自动响应上位机中断并将这个数值发送出去。

其他说明
	请不要以过快的速度请求数据，如需高速发送超过1KB/s速率的数据，请修改程序使用MCLK并设置更高波特率。上位机程序
	中每次请求设置了至少100ms的延时，如需更快速度，请另行修改或编写其他上位机。
	请尽量绑定全局变量，否则变量失效后读取结果将不可预测

关于程序
	Author：tanxiaoyao
	HomePage:http://git.oschina.net/tanxiaoyao/FLUKE45_VIEWR_RELEASE
	ReleaseDate:2015.5.11